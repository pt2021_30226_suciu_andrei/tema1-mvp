package view;

public interface IEmployeeView {
    public void afisMedicines(String afis);
    public String getManufacturerName();
    public String getInstructions();
    public String getNumOfCopies();
    public String getBarCode();
    public String getMedicineAmount();
    public String getPrice();
    public String getNumOfSoldCopies();
    public String getMedicineName();
    public String getPharmacyName();

    public String getUpdManufacturerName();
    public String getUpdInstructions();
    public String getUpdNumOfCopies();
    public String getUpdBarCode();
    public String getUpdMedicineAmount();
    public String getUpdPrice();
    public String getUpdNumOfSoldCopies();
    public String getUpdMedicineName();
    public String getUpdPharmacyName();

}
