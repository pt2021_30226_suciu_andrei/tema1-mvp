package view;

public interface IUserLogin {
    String getUserName();
    String getPassword();
}
