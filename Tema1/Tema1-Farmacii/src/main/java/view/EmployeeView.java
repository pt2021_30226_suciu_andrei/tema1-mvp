package view;

import presenter.EmployeePresenter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EmployeeView extends JFrame implements IEmployeeView {
    private JTextArea textArea1;
    private JButton deleteButton;
    private JButton afisareMedicamenteButton;
    private JButton filtrareDupaProducatorButton;
    private JButton filtrareDupaDisponibilitateButton;
    private JButton addButton;
    private JButton updateButton;
    private JTextField numOfCopies;
    private JTextField updNumOfCopies;
    private JTextField manufacturerName;
    private JTextField updManufacturerName;
    private JTextField instructions;
    private JTextField updInstructions;
    private JTextField price;
    private JTextField updPrice;
    private JTextField medicineAmount;
    private JTextField updMedicineAmount;
    private JTextField numOfSoldCopies;
    private JTextField updNumOfSoldCopies;
    private JPanel medicineName;
    private JTextField updMedicineName;
    private JTextField pharmacyName;
    private JTextField updPharmacyName;
    private JPanel mainPanel;
    private JTextField barCode;
    private JTextField updBarCode;
    private JTextField medicineNameF;
    private JButton cautare;
    private JButton filtrareDupaPretButton;
    private JButton salvareInMaiMulteButton;
    private JButton afisareProduseDintrUnButton;

    public EmployeeView(){
        this.setTitle("Employee");
        this.setSize(1550,820);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.add(mainPanel);
        this.setVisible(true);
        EmployeePresenter employeePresenter = new EmployeePresenter(this);

        afisareMedicamenteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                employeePresenter.listMedicines();
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                employeePresenter.addMedicine();
            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                employeePresenter.deleteMedicine();
            }
        });
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                employeePresenter.updateMedicine();
            }
        });
        cautare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                employeePresenter.searchByMedicineName();
            }
        });
        filtrareDupaDisponibilitateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                employeePresenter.filterByNumOfCopies();
            }
        });
        filtrareDupaProducatorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                employeePresenter.filterByManufacturerName();
            }
        });
        filtrareDupaPretButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                employeePresenter.filterByPrice();
            }
        });
        salvareInMaiMulteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                employeePresenter.saveAsOtherFiles();
            }
        });
        afisareProduseDintrUnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                employeePresenter.filterByPharmacy();
            }
        });
    }

    @Override
    public void afisMedicines(String afis) {
        this.textArea1.setText(afis);
    }

    @Override
    public String getManufacturerName() {
        return this.manufacturerName.getText();
    }

    @Override
    public String getInstructions() {
        return this.instructions.getText();
    }

    @Override
    public String getNumOfCopies() {
        return this.numOfCopies.getText();
    }

    @Override
    public String getBarCode() {
        return this.barCode.getText();
    }

    @Override
    public String getMedicineAmount() {
        return this.medicineAmount.getText();
    }

    @Override
    public String getPrice() {
        return this.price.getText();
    }

    @Override
    public String getNumOfSoldCopies() {
        return this.numOfSoldCopies.getText();
    }

    @Override
    public String getMedicineName() {
        return this.medicineNameF.getText();
    }

    @Override
    public String getPharmacyName() {
        return this.pharmacyName.getText();
    }

    @Override
    public String getUpdManufacturerName() {
        return this.updManufacturerName.getText();
    }

    @Override
    public String getUpdInstructions() {
        return this.updInstructions.getText();
    }

    @Override
    public String getUpdNumOfCopies() {
        return this.updNumOfCopies.getText();
    }

    @Override
    public String getUpdBarCode() {
        return this.updBarCode.getText();
    }

    @Override
    public String getUpdMedicineAmount() {
        return this.updMedicineAmount.getText();
    }

    @Override
    public String getUpdPrice() {
        return this.updPrice.getText();
    }

    @Override
    public String getUpdNumOfSoldCopies() {
        return this.updNumOfSoldCopies.getText();
    }

    @Override
    public String getUpdMedicineName() {
        return this.updMedicineName.getText();
    }

    @Override
    public String getUpdPharmacyName() {
        return this.updPharmacyName.getText();
    }
}
