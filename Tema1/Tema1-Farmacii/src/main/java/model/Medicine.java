package model;

public class Medicine {
    private String medicineName;
    private MedicineInfo medicineInfo;

    public Medicine() {
    }

    public Medicine(String medicineName, MedicineInfo medicineInfo) {
        this.medicineName = medicineName;
        this.medicineInfo = medicineInfo;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public MedicineInfo getMedicineInfo() {
        return medicineInfo;
    }

    public void getMedicineInfo(MedicineInfo medicineInfo) {
        this.medicineInfo = medicineInfo;
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "medicineName='" + medicineName + '\'' +
                ", medicineInfo=" + medicineInfo +
                '}';
    }
}
