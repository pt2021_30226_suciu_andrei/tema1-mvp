package model;

import java.util.ArrayList;
import java.util.List;

public class Pharmacy {
    private String pharmacyName;
    private List<Medicine> medicines;

    public Pharmacy(String pharmacyName, List<Medicine> medicines) {
        this.pharmacyName = pharmacyName;
        this.medicines = medicines;
    }

    public Pharmacy(String pharmacyName) {
        this.pharmacyName = pharmacyName;
        this.medicines = new ArrayList<>();
    }

    public String getPharmacyName() {
        return pharmacyName;
    }

    public void setPharmacyName(String pharmacyName) {
        this.pharmacyName = pharmacyName;
    }

    public List<Medicine> getMedicines() {
        return medicines;
    }

    public void setMedicines(List<Medicine> medicines) {
        this.medicines = medicines;
    }

    public void addMedicineToPharmacy(Medicine medicine){
        this.medicines.add(medicine);
    }

    public void removeMedicineFromPharmacy(Medicine medicine){
        this.medicines.remove(medicine);
    }

    @Override
    public String toString() {

        String result ="";

        result = "Pharmacy{" +
                "pharmacyName='" + pharmacyName + ",\n" +
                "medicines="+"\n";
        for(Medicine m: medicines){
            result+="   ";
            result+=m;
            result+="\n";
        }
        result+="}\n";

        return result;
    }
}
