package model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public interface MedicineJsonWriter {
    String fileName = "src/main/java/medicines.json";

    @SuppressWarnings("unchecked")
    default void saveAsJSON(List<Pharmacy> pharmacyList) {
        try {
            FileWriter file = new FileWriter(fileName);
            JSONObject obj ;

            for (Pharmacy p : pharmacyList) {
                obj = new JSONObject();
                obj.put("Pharmacy", p.getPharmacyName());
                for (Medicine m : p.getMedicines()) {
                    JSONArray company = new JSONArray();
                    company.add("MedicineName: " + m.getMedicineName());
                    company.add("ManufacturerName: " + m.getMedicineInfo().getManufacturerName());
                    company.add("Instructions: " + m.getMedicineInfo().getInstructions());
                    company.add("BarCode: " + m.getMedicineInfo().getBarCode());
                    company.add("NumberOfCopies: " + m.getMedicineInfo().getNumberOfCopies());
                    company.add("MedicineAmount: " + m.getMedicineInfo().getMedicineAmount());
                    company.add("Price: " + m.getMedicineInfo().getPrice());
                    company.add("NumberOfSoldCopies: " + m.getMedicineInfo().getNumberOfSoldCopies());
                    obj.put("MedicineSpecs: ", company);
                    file.write(obj.toJSONString());
                }
            }

            file.flush();
            file.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
