package model;

public class MedicineInfo {
    private String manufacturerName;
    private String instructions;
    private Integer barCode;
    private Integer numberOfCopies;
    private Integer medicineAmount;
    private Double price;
    private Integer numberOfSoldCopies;



    public MedicineInfo(String manufacturerName, String instructions, Integer barCode, Integer numberOfCopies,
                        Integer medicineAmount, Double price,  Integer numberOfSoldCopies) {
        this.manufacturerName = manufacturerName;
        this.instructions = instructions;
        this.barCode = barCode;
        this.numberOfCopies = numberOfCopies;
        this.medicineAmount = medicineAmount;
        this.price = price;
        this.numberOfSoldCopies = numberOfSoldCopies;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public Integer getNumberOfCopies() {
        return numberOfCopies;
    }


    public Integer getBarCode() {
        return barCode;
    }



    public Double getPrice() {
        return price;
    }



    public Integer getMedicineAmount() {
        return medicineAmount;
    }



    public Integer getNumberOfSoldCopies() {
        return numberOfSoldCopies;
    }


    @Override
    public String toString() {
        return "MedicineInfo{" +
                "manufacturerName='" + manufacturerName + '\'' +
                ", instructions='" + instructions + '\'' +
                ", barCode=" + barCode +
                ", numberOfCopies=" + numberOfCopies +
                ", medicineAmount=" + medicineAmount +
                ", price=" + price +
                ", numberOfSoldCopies=" + numberOfSoldCopies +
                '}';
    }
}
