package model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class PharmacyPersistencyAbs {

    public static Document getDocumentElements() {
        Document doc = null;
        try {

            File file = new File("src/main/java/medicines.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(file);

        } catch (Exception e) {
            e.printStackTrace();
            doc = null;
        }
        return doc;
    }

    public static List<Pharmacy> readXmlFile(){
        Document doc = getDocumentElements();
        doc.getDocumentElement().normalize();
        NodeList nodeList = doc.getElementsByTagName("medicine");
        List<Pharmacy> pharmacyList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) node;
                String pharmacyName = eElement.getElementsByTagName("pharmacyName").item(0).getTextContent();
                String medicineName = eElement.getElementsByTagName("medicineName").item(0).getTextContent();
                String manufacturerName = eElement.getElementsByTagName("manufacturerName").item(0).getTextContent();
                String instructions = eElement.getElementsByTagName("instructions").item(0).getTextContent();
                String barCode = eElement.getElementsByTagName("barCode").item(0).getTextContent();
                String numberOfCopies = eElement.getElementsByTagName("numberOfCopies").item(0).getTextContent();
                String medicineAmount  = eElement.getElementsByTagName("medicineAmount").item(0).getTextContent();
                String price = eElement.getElementsByTagName("price").item(0).getTextContent();
                String numberOfSoldCopies = eElement.getElementsByTagName("numberOfSoldCopies").item(0).getTextContent();

                MedicineInfo medicineInfo = new MedicineInfo(manufacturerName, instructions, Integer.parseInt(barCode), Integer.parseInt(numberOfCopies), Integer.parseInt(medicineAmount), Double.parseDouble(price),Integer.parseInt(numberOfSoldCopies));
                Medicine medicine = new Medicine(medicineName,medicineInfo);
                boolean gasit = false;
                int index = -1;
                for (Pharmacy p:pharmacyList) {
                    if(p.getPharmacyName().equals(pharmacyName)){
                        gasit=true;
                        index = pharmacyList.indexOf(p);
                    }
                }
                if(!gasit){
                    Pharmacy pharmacy = new Pharmacy(pharmacyName);
                    pharmacy.addMedicineToPharmacy(medicine);
                    pharmacyList.add(pharmacy);
                }else{
                    pharmacyList.get(index).addMedicineToPharmacy(medicine);
                }
            }
        }

        return pharmacyList;
    }

    public static void saveXmlContent(Document d) {
        String path = "src/main/java/medicines.xml";


        try {
            TransformerFactory tff = TransformerFactory.newInstance();
            Transformer tf = tff.newTransformer();
            tf.setOutputProperty(OutputKeys.ENCODING, "yes");
            DOMSource ds = new DOMSource(d);
            StreamResult sr = new StreamResult(path);
            tf.transform(ds, sr);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addMedicineToDocument(Pharmacy pharmacy){
        Document document = getDocumentElements();
        Element medicines = document.getDocumentElement();
        Element medicine = document.createElement("medicine");

        Element pharmacyName = document.createElement("pharmacyName");
        pharmacyName.appendChild(document.createTextNode(pharmacy.getPharmacyName()));
        medicine.appendChild(pharmacyName);

        List <Medicine> med = pharmacy.getMedicines();
        for (Medicine m : med) {
            Element medicineName = document.createElement("medicineName");
            medicineName.appendChild(document.createTextNode(m.getMedicineName()));
            medicine.appendChild(medicineName);

            Element manufacturerName = document.createElement("manufacturerName");
            manufacturerName.appendChild(document.createTextNode(m.getMedicineInfo().getManufacturerName()));
            medicine.appendChild(manufacturerName);

            Element instructions = document.createElement("instructions");
            instructions.appendChild(document.createTextNode(m.getMedicineInfo().getInstructions()));
            medicine.appendChild(instructions);

            Element barCode = document.createElement("barCode");
            barCode.appendChild(document.createTextNode(m.getMedicineInfo().getBarCode().toString()));
            medicine.appendChild(barCode);

            Element numberOfCopies = document.createElement("numberOfCopies");
            numberOfCopies.appendChild(document.createTextNode(m.getMedicineInfo().getNumberOfCopies().toString()));
            medicine.appendChild(numberOfCopies);

            Element medicineAmount = document.createElement("medicineAmount");
            medicineAmount.appendChild(document.createTextNode(m.getMedicineInfo().getMedicineAmount().toString()));
            medicine.appendChild(medicineAmount);

            Element price = document.createElement("price");
            price.appendChild(document.createTextNode(m.getMedicineInfo().getPrice().toString()));
            medicine.appendChild(price);

            Element numberOfSoldCopies = document.createElement("numberOfSoldCopies");
            numberOfSoldCopies.appendChild(document.createTextNode(m.getMedicineInfo().getNumberOfSoldCopies().toString()));
            medicine.appendChild(numberOfSoldCopies);

        }

        medicines.appendChild(medicine);
        saveXmlContent(document);
    }

    public static void deleteMedicineFromDocument(Pharmacy pharmacy){
        Document d = getDocumentElements();
        NodeList nl = d.getElementsByTagName("medicine");
        for (int i = 0; i < nl.getLength(); i++) {
            Element emedicine = (Element) nl.item(i);
            for (Medicine m:pharmacy.getMedicines()) {
                if(emedicine.getElementsByTagName("pharmacyName").item(0).getTextContent().equals(pharmacy.getPharmacyName()) &&
                        emedicine.getElementsByTagName("medicineName").item(0).getTextContent().equals(m.getMedicineName()) &&
                        emedicine.getElementsByTagName("manufacturerName").item(0).getTextContent().equals(m.getMedicineInfo().getManufacturerName()) ){
                    emedicine.getParentNode().removeChild(emedicine);
                }
            }
        }
        saveXmlContent(d);
    }


}
