package model;

import java.util.List;

public class PharmacyPersistency extends PharmacyPersistencyAbs implements MedicineCSVWriter,MedicineJsonWriter {
    private List<Pharmacy> pharmacyList;

    public PharmacyPersistency() {
        this.pharmacyList = readXmlFile();
    }

    public List<Pharmacy> getPharmacyList() {
        return pharmacyList;
    }

    public boolean addNewMedicine(Pharmacy pharmacy){
        Medicine medicine = pharmacy.getMedicines().get(0);
        for (Pharmacy p:pharmacyList) {

            if (p.getPharmacyName().equals(pharmacy.getPharmacyName())){

                for (Medicine m:p.getMedicines()) {
                    if (m.getMedicineName().equals(medicine.getMedicineName()) && m.getMedicineInfo().getManufacturerName().equals(medicine.getMedicineInfo().getManufacturerName())){
                        return false;
                    }
                }
                addMedicineToDocument(pharmacy);
                pharmacyList = readXmlFile();
                return true;
            }
        }
        addMedicineToDocument(pharmacy);
        pharmacyList = readXmlFile();
        return true;
    }

    public Pharmacy getPharmacyByStrings(String pharmacyName,String medicineName, String manufacturerName){
        Pharmacy pharmacy = null;
        for (Pharmacy p:pharmacyList) {
            if (p.getPharmacyName().equals(pharmacyName)){
                List <Medicine> medicines = p.getMedicines();
                for (Medicine m:medicines) {
                    if(m.getMedicineName().equals(medicineName) && m.getMedicineInfo().getManufacturerName().equals(manufacturerName)){
                        pharmacy = new Pharmacy(p.getPharmacyName());
                        pharmacy.addMedicineToPharmacy(m);
                    }
                }
            }
        }

        return pharmacy;
    }

    public void deleteMedicine(Pharmacy pharmacy) {
        deleteMedicineFromDocument(pharmacy);
        pharmacyList = readXmlFile();
    }

    public void updateMedicine(Pharmacy oldMedicine, Pharmacy newMedicine){
        deleteMedicine(oldMedicine);
        addNewMedicine(newMedicine);
    }

    @Override
    public String toString() {
        String result ="";

        for (Pharmacy p:pharmacyList) {
            result += p+"\n";
        }
        return result;
    }

}
