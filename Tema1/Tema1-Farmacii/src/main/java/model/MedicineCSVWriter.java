package model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public interface MedicineCSVWriter {
    String fileName = "src/main/java/medicines.csv";
    default void saveAsCSV(List<Pharmacy> pharmacyList){
        try (PrintWriter writer = new PrintWriter(fileName)) {
            StringBuilder sb = new StringBuilder();
            sb.append("pharmacyName");
            sb.append(',');
            sb.append("medicineName");
            sb.append(',');
            sb.append("manufacturerName");
            sb.append(',');
            sb.append("instructions");
            sb.append(',');
            sb.append("barCode");
            sb.append(',');
            sb.append("numberOfCopies");
            sb.append(',');
            sb.append("medicineAmount");
            sb.append(',');
            sb.append("price");
            sb.append(',');
            sb.append("numberOfSoldCopies");
            sb.append('\n');

            for (Pharmacy p : pharmacyList) {
                for (Medicine m : p.getMedicines()) {
                    sb.append(p.getPharmacyName());
                    sb.append(',');
                    sb.append(m.getMedicineName());
                    sb.append(',');
                    sb.append(m.getMedicineInfo().getManufacturerName());
                    sb.append(',');
                    sb.append(m.getMedicineInfo().getInstructions());
                    sb.append(',');
                    sb.append(m.getMedicineInfo().getBarCode().toString());
                    sb.append(',');
                    sb.append(m.getMedicineInfo().getNumberOfCopies().toString());
                    sb.append(',');
                    sb.append(m.getMedicineInfo().getMedicineAmount().toString());
                    sb.append(',');
                    sb.append(m.getMedicineInfo().getPrice().toString());
                    sb.append(',');
                    sb.append(m.getMedicineInfo().getNumberOfSoldCopies().toString());
                    sb.append('\n');
                }
            }
            writer.write(sb.toString());
        }catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
