package presenter;

import model.User;
import model.UserPersistency;
import view.AdministratorView;
import view.EmployeeView;
import view.UserLogin;
import javax.swing.*;


public class UserPresenter {
    private UserLogin userLogin;
    private UserPersistency userPersistency;

    public UserPresenter(UserLogin userLogin) {
        this.userLogin = userLogin;
        this.userPersistency = new UserPersistency();
    }

    public void actionListenerLogin() {
        String userName, password;
        userName = this.userLogin.getUserName();
        password = this.userLogin.getPassword();
        User user = userPersistency.getUserByUsername(userName);

        if (user != null) {
            if (user.getPassword().equals(password)) {
                if(user.getRole().equals("administrator")){
                    new AdministratorView();
                }else{
                    new EmployeeView();
                }
                this.userLogin.setVisible(false);
            } else {
                JOptionPane.showMessageDialog(null, "Parola gresita!");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nu exista acest username!");
        }
    }
}
