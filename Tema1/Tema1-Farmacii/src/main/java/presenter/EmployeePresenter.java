package presenter;

import model.Medicine;
import model.MedicineInfo;
import model.PharmacyPersistency;
import model.Pharmacy;
import view.EmployeeView;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeePresenter {
    EmployeeView view;
    PharmacyPersistency pharmacyPersistency;

    public EmployeePresenter(EmployeeView view) {
        this.view = view;
        pharmacyPersistency = new PharmacyPersistency();
    }


    public void listMedicines() {
        view.afisMedicines(pharmacyPersistency.toString());
    }

    public void addMedicine() {
        String pharmacyName = view.getPharmacyName();
        String medicineName = view.getMedicineName();
        String manufacturerName = view.getManufacturerName();
        String instructions = view.getInstructions();
        String barCode = view.getBarCode();
        String numberOfCopies = view.getNumOfCopies();
        String medicineAmount = view.getMedicineAmount();
        String price = view.getPrice();
        String numberOfSoldCopies = view.getNumOfSoldCopies();

        Integer nofc = null, barC = null, medAmo = null, nofsc = null;
        Double pri = null;
        try {
            nofc = Integer.parseInt(numberOfCopies);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Adaugati un numar intreg in campul Numar produse ramase!");
        }

        try {
            barC = Integer.parseInt(barCode);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Adaugati un numar intreg in campul Cod de bare!");
        }

        try {
            pri = Double.parseDouble(price);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Adaugati un numar real in campul Pret!");
        }

        try {
            medAmo = Integer.parseInt(medicineAmount);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Adaugati un numar intreg in campul Dimensiunea in mg/ml!");
        }

        try {
            nofsc = Integer.parseInt(numberOfSoldCopies);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Adaugati un numar intreg in campul Numar produse vandute!");
        }

        if (nofc != null && barC != null && pri != null && medAmo != null && nofsc != null) {
            if (pharmacyName.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume farmacie!");
            } else {
                if (medicineName.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume medicament!");
                } else {
                    if (manufacturerName.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume producator!");
                    } else {
                        if (instructions.isEmpty()) {
                            JOptionPane.showMessageDialog(null, "Adaugati un string in campul Instructiuni!");
                        } else {

                            MedicineInfo medicineInfo = new MedicineInfo(manufacturerName, instructions, barC, nofc, medAmo, pri, nofsc);
                            Medicine medicine = new Medicine(medicineName, medicineInfo);
                            Pharmacy pharmacy = new Pharmacy(pharmacyName);
                            pharmacy.addMedicineToPharmacy(medicine);

                            if (!pharmacyPersistency.addNewMedicine(pharmacy)) {
                                JOptionPane.showMessageDialog(null, "Nu a putut fi adaugat acest medicament deoarece exista deja!");
                            }
                        }
                    }
                }
            }
        }
    }

    public void deleteMedicine(){
        String pharmacyName = view.getPharmacyName();
        String medicineName = view.getMedicineName();
        String manufacturerName = view.getManufacturerName();

        if (pharmacyName.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume farmacie pentru a realiza stergerea!");
        } else {
            if (medicineName.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume Medicament pentru a realiza stergerea!");
            } else {
                if (manufacturerName.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume producator pentru a realiza stergerea!");
                } else {
                    Pharmacy pharmacy = pharmacyPersistency.getPharmacyByStrings(pharmacyName, medicineName, manufacturerName);
                    if (pharmacy == null) {
                        JOptionPane.showMessageDialog(null, "Medicament negasit, nu poate avea loc stergerea!");
                    } else {
                        pharmacyPersistency.deleteMedicine(pharmacy);
                    }
                }
            }
        }
    }

    public void updateMedicine() {
        String pharmacyName = view.getPharmacyName();
        String medicineName = view.getMedicineName();
        String manufacturerName = view.getManufacturerName();

        if (pharmacyName.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume farmacie pentru a realiza actualizarea!");
        } else {
            if (medicineName.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume Medicament pentru a realiza actualizarea!");
            } else {
                if (manufacturerName.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume producator pentru a realiza actualizarea!");
                } else {
                    Pharmacy pharmacyPersistencyOld = pharmacyPersistency.getPharmacyByStrings(pharmacyName, medicineName, manufacturerName);
                    if (pharmacyPersistencyOld == null) {
                        JOptionPane.showMessageDialog(null, "Medicament negasit, nu poate avea loc actualizarea!");
                    } else {

                        String pharmacyNameNew = view.getUpdPharmacyName();
                        String medicineNameNew = view.getUpdMedicineName();
                        String manufacturerNameNew = view.getUpdManufacturerName();
                        String instructionsNew = view.getUpdInstructions();
                        String numberOfCopiesNew = view.getUpdNumOfCopies();
                        String barCodeNew = view.getUpdBarCode();
                        String priceNew = view.getUpdPrice();
                        String medicineAmountNew = view.getUpdMedicineAmount();
                        String numberOfSoldCopiesNew = view.getUpdNumOfSoldCopies();

                        if (pharmacyNameNew.isEmpty()) {
                            pharmacyNameNew = pharmacyPersistencyOld.getPharmacyName();
                        }
                        if (medicineNameNew.isEmpty()) {
                            medicineNameNew = pharmacyPersistencyOld.getMedicines().get(0).getMedicineName();
                        }
                        if (manufacturerNameNew.isEmpty()) {
                            manufacturerNameNew = pharmacyPersistencyOld.getMedicines().get(0).getMedicineInfo().getManufacturerName();
                        }
                        if (instructionsNew.isEmpty()) {
                            instructionsNew = pharmacyPersistencyOld.getMedicines().get(0).getMedicineInfo().getInstructions();
                        }
                        if (numberOfCopiesNew.isEmpty()) {
                            numberOfCopiesNew = pharmacyPersistencyOld.getMedicines().get(0).getMedicineInfo().getNumberOfCopies().toString();
                        }
                        if (barCodeNew.isEmpty()) {
                            barCodeNew = pharmacyPersistencyOld.getMedicines().get(0).getMedicineInfo().getBarCode().toString();
                        }
                        if (priceNew.isEmpty()) {
                            priceNew = pharmacyPersistencyOld.getMedicines().get(0).getMedicineInfo().getPrice().toString();
                        }
                        if (medicineAmountNew.isEmpty()) {
                            medicineAmountNew = pharmacyPersistencyOld.getMedicines().get(0).getMedicineInfo().getMedicineAmount().toString();
                        }
                        if (numberOfSoldCopiesNew.isEmpty()) {
                            numberOfSoldCopiesNew = pharmacyPersistencyOld.getMedicines().get(0).getMedicineInfo().getNumberOfSoldCopies().toString();
                        }

                        Integer nofc = null, barC = null, medAmo = null, nofsc = null;
                        Double pri = null;
                        try {
                            nofc = Integer.parseInt(numberOfCopiesNew);
                        } catch (NumberFormatException e) {
                            JOptionPane.showMessageDialog(null, "Adaugati un numar intreg in campul Numar produse ramase!");
                        }

                        try {
                            barC = Integer.parseInt(barCodeNew);
                        } catch (NumberFormatException e) {
                            JOptionPane.showMessageDialog(null, "Adaugati un numar intreg in campul Cod de bare!");
                        }

                        try {
                            pri = Double.parseDouble(priceNew);
                        } catch (NumberFormatException e) {
                            JOptionPane.showMessageDialog(null, "Adaugati un numar real in campul Pret!");
                        }

                        try {
                            medAmo = Integer.parseInt(medicineAmountNew);
                        } catch (NumberFormatException e) {
                            JOptionPane.showMessageDialog(null, "Adaugati un numar intreg in campul Dimensiunea in mg/ml!");
                        }

                        try {
                            nofsc = Integer.parseInt(numberOfSoldCopiesNew);
                        } catch (NumberFormatException e) {
                            JOptionPane.showMessageDialog(null, "Adaugati un numar intreg in campul Numar produse vandute!");
                        }

                        if (nofc != null && barC != null && pri != null && medAmo != null && nofsc != null) {

                            MedicineInfo medicineInfo = new MedicineInfo(manufacturerNameNew, instructionsNew, barC, nofc, medAmo, pri, nofsc);
                            Medicine medicine = new Medicine(medicineNameNew, medicineInfo);
                            Pharmacy pharmacyNew = new Pharmacy(pharmacyNameNew);
                            pharmacyNew.addMedicineToPharmacy(medicine);

                            pharmacyPersistency.updateMedicine(pharmacyPersistencyOld, pharmacyNew);

                        }
                    }
                }
            }
        }
    }

    public void searchByMedicineName() {
        String medicineName = view.getMedicineName();
        if (medicineName.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume medicament pentru a realiza cautare dupa nume!");
        } else {
            List<Pharmacy> pharmacyList = pharmacyPersistency.getPharmacyList();

            List<Pharmacy> medicineToView = new ArrayList<>();

            for (Pharmacy p : pharmacyList) {
                List<Medicine> medicines = p.getMedicines();
                for (Medicine m : medicines) {
                    if (m.getMedicineName().equals(medicineName)) {
                        Pharmacy med = new Pharmacy(p.getPharmacyName());
                        med.addMedicineToPharmacy(m);
                        medicineToView.add(med);
                    }
                }
            }

            if (medicineToView.size() == 0) {
                view.afisMedicines("Nu s-a gasit nimic!");
            } else {
                view.afisMedicines(medicineToView.toString());
            }
        }
    }

    public void filterByNumOfCopies() {

        List<Pharmacy> pharmacyList = pharmacyPersistency.getPharmacyList();
        List<Medicine> deAfisat;
        List<Pharmacy> medicineToView = new ArrayList<>();
        for (Pharmacy p : pharmacyList) {
            deAfisat = p.getMedicines().stream().filter((m) -> m.getMedicineInfo().getNumberOfCopies() > 0).collect(Collectors.toList());
            if (deAfisat.size() > 0){
                Pharmacy med = new Pharmacy(p.getPharmacyName());
                med.setMedicines(deAfisat);
                medicineToView.add(med);
            }
        }

        if (medicineToView.size() == 0) {
            view.afisMedicines("Nu s-a gasit nimic!");
        } else {
            view.afisMedicines(medicineToView.toString());
        }

    }

    public void filterByManufacturerName() {
        String manufacturerName = view.getManufacturerName();
        if(manufacturerName.isEmpty()){
            JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume producator pentru a realiza filtrarea!");
        }else{

            List<Pharmacy> pharmacyList = pharmacyPersistency.getPharmacyList();
            List<Medicine> deAfisat;
            List<Pharmacy> medicineToView = new ArrayList<>();
            for (Pharmacy p : pharmacyList) {
                deAfisat = p.getMedicines().stream().filter((m) -> m.getMedicineInfo().getManufacturerName().equals(manufacturerName)).collect(Collectors.toList());
                if (deAfisat.size() > 0){
                    Pharmacy med = new Pharmacy(p.getPharmacyName());
                    med.setMedicines(deAfisat);
                    medicineToView.add(med);
                }
            }

            if (medicineToView.size() == 0) {
                view.afisMedicines("Nu s-a gasit nimic!");
            } else {
                view.afisMedicines(medicineToView.toString());
            }

        }
    }

    public void filterByPrice() {
        String price1 = view.getPrice();
        String price2 = view.getUpdPrice();
        Double pri1=null,pri2=null;
        try {
            pri1 = Double.parseDouble(price1);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Adaugati un numar real in primul camp Pret!");
        }
        try {
            pri2 = Double.parseDouble(price2);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Adaugati un numar real in al doilea camp Pret!");
        }
        if (pri1 != null && pri2 != null){
            if(pri1 > pri2){
                Double aux = pri1;
                pri1 = pri2;
                pri2 = aux;
            }

            List<Pharmacy> pharmacyList = pharmacyPersistency.getPharmacyList();
            List<Medicine> deAfisat;
            List<Pharmacy> medicineToView = new ArrayList<>();
            for (Pharmacy p : pharmacyList) {
                Double finalPri = pri1;
                Double finalPri1 = pri2;
                deAfisat = p.getMedicines().stream().filter((m) -> m.getMedicineInfo().getPrice()>=finalPri && m.getMedicineInfo().getPrice()<=finalPri1).collect(Collectors.toList());
                if (deAfisat.size() > 0){
                    Pharmacy med = new Pharmacy(p.getPharmacyName());
                    med.setMedicines(deAfisat);
                    medicineToView.add(med);
                }
            }

            if (medicineToView.size() == 0) {
                view.afisMedicines("Nu s-a gasit nimic!");
            } else {
                view.afisMedicines(medicineToView.toString());
            }
        }
    }

    public void saveAsOtherFiles() {
        pharmacyPersistency.saveAsCSV(pharmacyPersistency.getPharmacyList());
        pharmacyPersistency.saveAsJSON(pharmacyPersistency.getPharmacyList());
    }

    public void filterByPharmacy() {
        String pharmacyName = view.getPharmacyName();
        if (pharmacyName.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Adaugati un nume in campul Nume farmacie pentru a realiza afisarea medicamentelor dintr-o farmacie!");
        }else{
            List<Pharmacy> toList = new ArrayList<>();
            toList = pharmacyPersistency.getPharmacyList().stream().filter(pharmacy -> pharmacy.getPharmacyName().equals(pharmacyName)).collect(Collectors.toList());

            if (toList.size() == 0) {
                view.afisMedicines("Nu s-a gasit nimic!");
            } else {
                view.afisMedicines(toList.toString());
            }
        }

    }
}
