package presenter;

import model.User;
import model.UserPersistency;
import view.AdministratorView;

import javax.swing.*;

public class AdministratorPresenter {
    private AdministratorView view;
    private UserPersistency userPersistency;

    public AdministratorPresenter(AdministratorView view) {
        this.view = view;
        this.userPersistency = new UserPersistency();
    }

    public void listEmployee() {
        view.afisUsers(userPersistency.toString());
    }

    public void addNewUser() {
        String username = view.getUserNameFromTField();
        String password = view.getPasswordFromTField();
        String role = view.getRoleFromTField();

        if (username.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Adaugati un nume de utilizator!");
        } else {
            if (password.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Adaugati o parola!");
            } else if (role.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Adaugati un rol pentru acest utilizator!");
            } else {
                User user = new User(username, password, role);
                if (!userPersistency.addUser(user)) {
                    JOptionPane.showMessageDialog(null, "Nu a putut fi adaugat acest utilizator, deoarece exista deja!");
                }
            }
        }
    }

    public void deleteUser() {
        String username = view.getUserNameFromTField();
        if (username.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Adaugati un nume de utilizator pentru a realiza stergerea!");
        } else {
            User user = userPersistency.getUserByUsername(username);
            if (user == null) {
                JOptionPane.showMessageDialog(null, "Nu exista acest utilizator!");
            } else {
                userPersistency.removeUser(user);
            }
        }

    }

    public void updateUser() {
        String username = view.getUserNameFromTField();
        if (username.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Adaugati un nume de utilizator pentru a realiza actualizarea!");
        } else {
            User user = userPersistency.getUserByUsername(username);
            if (user == null) {
                JOptionPane.showMessageDialog(null, "Nu exista acest utilizator !");
            } else {
                String newUsername = view.getUpdateUserNameFromTField();
                String newPassword = view.getUpdatePasswordFromTField();
                String newRole = view.getUpdateRoleFromTField();
                if (newUsername.isEmpty() && newPassword.isEmpty() && newRole.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Nu ati introdus niciun camp pentru actualizare!");
                } else {
                    if (newUsername.isEmpty()) {
                        newUsername = user.getUserName();
                    }
                    if (newPassword.isEmpty()) {
                        newPassword = user.getPassword();
                    }
                    if (newRole.isEmpty()) {
                        newRole = user.getRole();
                    }
                    User newUser = new User(newUsername, newPassword, newRole);
                    userPersistency.updateUser(user, newUser);
                }
            }
        }
    }
}
